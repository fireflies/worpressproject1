<!DOCTYPE html>
<html <?php language_attributes()?>>
	<head>
		<meta chartset="<?php bloginfo('chartset');?>">
		<meta name="viewport" content="width-device-width">
		<title><?php bloginfo('name'); ?></title>
		<?php wp_head(); ?>
	</head>

	<body class="bg-light">
					<div class="container">
			<header>
				<h1><a href="<?php echo home_url();?>"><?php bloginfo('name')?></a>
				<p><?php bloginfo('description')?></p>
			</header>

			<nav class="navbar navbar-expand-lg ">
			
				<?php
					$args = array(
						'theme_location' => 'primary'
					)
				?>
				<?php wp_nav_menu( $args )?>
			</nav>