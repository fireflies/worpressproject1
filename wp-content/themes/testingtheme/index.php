<?php
		get_header();?>


	<form role="search" method="get" id="searchform"
    class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	    <div>
	        <label class="screen-reader-text" for="s"><?php _x( 'Search for:', 'label' ); ?></label>
	        <input type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" /><br>
	        <input type='checkbox' name='category' value='pinoy'<?php checked( $category, 'pinoy'); ?> />
	        <input type="submit" id="searchsubmit"
	            value="<?php echo esc_attr_x( 'Search', 'submit button' ); ?>" />
	    </div>
	</form>

	<?php

	
	if($_GET['category'] == 'pinoy'){
		$args = array(
		'cat' => '2',
		's' => $_GET['s']
	);
		$query = new WP_Query( $args );
	if($query->have_posts()):
		while($query->have_posts()): $query->the_post();
		?>
		<div class="card">
			<h2 class="card-title"><?php the_title(); ?></h2>
  			<p class="card-text"><?php the_content(); ?></p>
		</div>
		<?php
	endwhile;
else:
	echo "<p>no content</p>";
endif;
	}
	if(have_posts()):
		while(have_posts()): the_post();
		?>
		<div class="card">
			<h2 class="card-title"><?php the_title(); ?></h2>
  			<p class="card-text"><?php the_content(); ?></p>
		</div>
		<?php
	endwhile;
else:
	echo "<p>no content</p>";
endif;
	get_footer();
?>