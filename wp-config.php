<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'demo' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '/[GMz!kXYymUo.+Y)^iNNzI<@~9r9|2&$+P(.hv9C61Q(5_*og7~bqBeWi}}(BIn' );
define( 'SECURE_AUTH_KEY',  'D0s2>,4+w}h:N5n.>5%cw-L4HjGT^,0URUNKzEieGJ=zS3T#UU#G /N2-6MpU:S2' );
define( 'LOGGED_IN_KEY',    '5+/}mop(?j+N y?edS@EFjUv2$x9rv{1J>#@9l*ePwa?iQ*7jg_b[#^N;K^T03n+' );
define( 'NONCE_KEY',        'tMSFY0 0?P}PS6q%j_G~44(n!2(9?Yo2H+5DybSjjl1S?] YD^OI/dg(D=9Y3:)S' );
define( 'AUTH_SALT',        '[t3@]0[yce#w+Jh3x^D+xRP^WmFg-}vW$t!9f[wXns54ZWS;}DtkN)ujcMWbGxnM' );
define( 'SECURE_AUTH_SALT', '(FWubt=qU.r2WI]dKL+~cMDS[GQcd)<8y|f^:+xYUyw!1<XanacJdo<^O9N|v78*' );
define( 'LOGGED_IN_SALT',   'V0YK?$eC:9r*&V&4r>)aL/}0e?glIPUhL$A1O:K-t_uNtg-eig*.hbdG9#rJ:?pl' );
define( 'NONCE_SALT',       'wZaHo Sz5%^wOk,X-{O=ir#$@}s4<M/>7U,9HKm(UYk{rqezRLqkhccaNg}6:QIT' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
